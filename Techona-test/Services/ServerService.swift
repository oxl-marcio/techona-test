//
//  ServerService.swift
//  Techona-test
//
//  Created by Marcio Garcia on 2/2/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import Foundation

enum ButtonType: Int {
    case Apple
    case Google
    case Yahoo
}

class ServerService {
    
    static let instance = ServerService()
    
    var buttons: [ButtonType] = [
        ButtonType.Apple,
        ButtonType.Google,
        ButtonType.Yahoo,
    ]
    
    private init(){}
    
    func getButtonList(completion: (buttons: [ButtonType]?, error: NSError?) -> Void) {
        
        // Retrieve buttons from the server with an asyncronous http call
        // Calls our completion clousure from the clousure of the http call
        // Checks if the http call returns any error and pass it to our completion handler
        completion(buttons: self.buttons, error: nil)
    }
    
}
