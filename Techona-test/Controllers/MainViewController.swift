////
//  MainViewController.swift
//  Techona-test
//
//  Created by Marcio Garcia on 2/2/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    var customButton = [BaseButton]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ServerService.instance.getButtonList { (buttons, error) in
            
            if let buttonList = buttons {
                
                for button in buttonList {
                    
                    var previousButtonOriginY: CGFloat = 0.0
                    var previousButtonHeight: CGFloat = 0.0
                    
                    let count = self.customButton.count
                    if count > 0 {
                        let previousButton = self.customButton[count-1]
                        previousButtonOriginY = previousButton.origin.y
                        previousButtonHeight = previousButton.size.height
                    }
                    
                    let posY = previousButtonOriginY + previousButtonHeight + 50
                    
                    switch button {
                        case .Apple:
                            let b = AppleButton(name: "Apple", title: "Tap Here", origin: CGPoint(x: 40, y: posY), owner: self)
                            self.customButton.append(b)
                            self.view.addSubview(b.view)
                        case .Google:
                            let b = GoogleButton(name: "Google", title: "Tap Me", origin: CGPoint(x: 40, y: posY), owner: self)
                            self.customButton.append(b)
                            self.view.addSubview(b.view)
                        case .Yahoo:
                            let b = YahooButton(name: "Yahoo", title: "Tap Fever", origin: CGPoint(x: 40, y: posY), owner: self)
                            self.customButton.append(b)
                            self.view.addSubview(b.view)
                    }
                    
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
