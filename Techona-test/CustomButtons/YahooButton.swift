//
//  YahooButton.swift
//  Techona-test
//
//  Created by Marcio Garcia on 2/3/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import UIKit

class YahooButton: BaseButton {
    
    override init(name: String, title: String, origin: CGPoint, owner: UIViewController) {
        super.init(name: name, title: title, origin: origin, owner: owner)
        
        self.addButtonElements()
    }
    
    func addButtonElements() {
        
        let buttonSize = CGSize(width: 100, height: 40)
        let buttonOrigin = CGPoint(x: 30.0, y: (self.view.frame.height / 2) - (buttonSize.height / 2))
        let button = self.createButton(buttonOrigin, size: buttonSize)
        button.addTarget(self, action: #selector(buttonTapped), forControlEvents: .TouchUpInside)
        
        let labelSize = CGSize(width: 100, height: 21)
        let labelOrigin = CGPoint(x: buttonOrigin.x + buttonSize.width + 10.0, y: (self.view.frame.height / 2) - (labelSize.height / 2))
        let label = self.createLabel(labelOrigin, size: labelSize)
        
        self.view.addSubview(label)
        self.view.addSubview(button)
        
    }
    
    @objc func buttonTapped() {
        self.showButtonName()
    }
}

