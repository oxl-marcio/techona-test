//
//  Button.swift
//  Techona-test
//
//  Created by Marcio Garcia on 2/2/17.
//  Copyright © 2017 Marcio Garcia. All rights reserved.
//

import UIKit

class BaseButton {
    private var _name: String!
    private var _title: String!
    private var _owner: UIViewController!
    private var _size: CGSize!
    
    var view: UIView!
    var origin: CGPoint!
    
    var name: String {
        get {
            return self._name
        }
    }

    var title: String {
        get {
            return self._title
        }
    }

    var owner: UIViewController {
        get {
            return self._owner
        }
    }
    
    var size: CGSize {
        get {
            return self._size
        }
    }

    init(name: String, title: String, origin: CGPoint, owner: UIViewController) {
        self._name = name
        self._title = title
        self._owner = owner
        self.origin = origin
        self._size = CGSize(width: 250, height: 100)
        
        self.defineContainerViewLayout()
    }
    
    func defineContainerViewLayout() {
        
        let containerView = UIView(frame: CGRect(origin: self.origin, size: self._size))
        containerView.userInteractionEnabled = true
        containerView.backgroundColor = UIColor.whiteColor()
        containerView.layer.cornerRadius = 4.0
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.2).CGColor
        containerView.center.x = self._owner.view.center.x
        
        self.view = containerView
    }
    
    func createLabel(origin: CGPoint, size: CGSize) -> UILabel {
        
        let label = UILabel(frame: CGRect(origin: origin, size: size))
        label.text = name
        label.font = UIFont.systemFontOfSize(16.0, weight: UIFontWeightBold)
        label.textAlignment = .Center
        
        return label
    }
    
    func createButton(origin: CGPoint, size: CGSize) -> UIButton {
        
        let button = UIButton(frame: CGRect(origin: origin, size: size))
        button.userInteractionEnabled = true
        button.setTitle(title, forState: .Normal)
        button.backgroundColor = UIColor(red: 0, green: 122/255, blue: 1, alpha: 1)
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        button.titleLabel?.font = UIFont.systemFontOfSize(13.0, weight: UIFontWeightRegular)
        button.layer.cornerRadius = 4.0
        
        return button
    }

    func showButtonName() {
        let alert = UIAlertController(title: self.name, message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.owner.presentViewController(alert, animated: true, completion: nil)
    }
    
}